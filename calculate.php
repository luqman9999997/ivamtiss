<!DOCTYPE html>
<html>
<head>
	<title>Electricity Calculator</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
	<div class="container mt-4">
		<h1>Electricity Calculator</h1>
		<form method="post">
			<div class="form-group">
				<label for="voltage">Voltage (V):</label>
				<input type="number" class="form-control" id="voltage" name="voltage" required>
			</div>
			<div class="form-group">
				<label for="current">Current (A):</label>
				<input type="number" class="form-control" id="current" name="current" required>
			</div>
			<div class="form-group">
				<label for="rate">Rate (per kWh):</label>
				<input type="number" class="form-control" id="rate" name="rate" required>
			</div>
			<button type="submit" class="btn btn-primary">Calculate</button>
		</form>

		<?php
			if ($_SERVER['REQUEST_METHOD'] == 'POST') {
				$voltage = $_POST['voltage'];
				$current = $_POST['current'];
				$rate = $_POST['rate'];

				$power = $voltage * $current;
				$energy = $power * 1 * 1000; // assuming 1 hour
				$total = $energy * ($rate / 100);

				echo '<hr>';
				echo '<h2>Results</h2>';
				echo '<p>Power: ' . $power . ' W</p>';
				echo '<p>Energy: ' . ($energy / 1000) . ' kWh</p>';
				echo '<p>Total Charge: RM' . number_format($total, 2) . '</p>';
			}
		?>
	</div>
</body>
</html>
